<title>Grand oral : pistes de sujets</title>

<h1> Grand oral : pistes de sujets </h1>

Le candidat choisit un sujet, motivé par sa **curiosité
intellectuelle**

ou
résultant, selon les disciplines, d’un **projet
mené en équipe**.

Le sujet peut être en lien avec un **projet d’études supérieures**

et une potentielle **orientation professionnelle**.

Le candidat donne un titre à sa prestation sous forme de **question**,

qui contient la **problématique**.

La question engage le candidat dans sa relation au sujet :

En quoi le sujet constitue-t-il une **question vive** ?

Avec quelles **implications** dans le monde ?

Thématique transversale empruntée  au monde contemporain : 

- La
   santé (imagerie médicale, robotique, bio-informatique…)

- L’économie
   (crypto-monnaies,…)

- Le
   droit (informatique et liberté,…)

- L’art
   et la culture (art algorithmique, IA compositeur,…)

- L’environnement
   et le développement durable (Green computing,…)

- Les
   sciences (instrumentation, …)

- Les
   langues (traduction automatique, correcteur automatique,…)

- Le
   sport (e-sport,…)

- Les
   loisirs (jeux vidéos,…)

**La communication de l’information :** 

- internet, 

- sites
   web, 

- réseaux
   sociaux,

- **Les  données informatiques :** 
  
  - Protection
     et sécurisation des données,
  
  - Protection
     des libertés individuelles,
  
  - Cybersécurité,
  
  - Traitement
     des données,
  
  - Analyse
     des données, Big Data,
  
  - Les
     bugs,
  
  - La
     cryptographie,
  
  - Le
     stockage des données,

- **L'évolution  de l’informatique :** 
  
  - évolution
     matérielle, 
  
  - loi
     de Moore, 
  
  - évolution
     des langages, 
  
  - évolution
     de la robotique,

- **Les  innovations dans le domaine de l'informatique :** 
  
  - les
     ordinateurs quantiques, 
  
  - les
     super-calculateurs
  
  - IoT, 
  
  - les
     smart-cities, 
  
  - la
     réalité virtuelle, 
  
  - les
     véhicules autonomes, 
  
  - l’intelligence
     artificielle,
  
  - la
     domotique,

# Partie transversale : L’histoire de l’informatique

- Le
   numérique : facteur de démocratisation ou de fractures sociales ?
   (~SES)

- Informatique
   : quel impact sur le climat ? (~SVT)

- L’économie
   du numérique (~SES)

- Femmes
   et numérique : quelle histoire ? quel avenir ?

- Ada
   Lovelace, pionnière du langage informatique

- Alan
   Turing, et l’informatique fut

# NSI et projets d’orientation

- L’informatique
   va-t-elle révolutionner le dessin animé ?

- L’informatique
   va-t-elle révolutionner la composition musicale ?

- L’informatique
   va-t-elle révolutionner l’art ?

- L’informatique
   va-t-elle révolutionner le cinéma ?

- L’informatique
   va-t-elle révolutionner la médecine ? (~SVT)

- L’informatique
   va-t-elle révolutionner la physique ? (~PC, Maths)

- L’informatique
   va-t-elle révolutionner l’entreprise ?

- La
   bio informatique (~SVT)

- Jeux
   vidéos...

# Partie 1 : Langages et programmation

- P  = NP, un problème à un million de dollars ? (~Maths)

## Récursivité

- Tours
   de Hanoï : plus qu’un jeu d’enfants ? (~Maths)

- Les
   fractales : Informatique et mathématiques imitent-elles la nature ?
   (~SVT, Maths)

- De
   la récurrence à la récursivité (~Maths)

## Mise au point des

programmes

- Les
   bugs : bête noire des développeurs ? (~Maths)

- Comment
   rendre l’informatique plus sûre ? (~Maths)

# Partie 2 : Données structurées et

structures de données

- Cartographie
   (~Maths, PC)

- L’informatisation
   des métros (~Maths, PC)

- Compression
   des données

- Musique
   et informatique : des sons et des bits

# Partie 3 : Algorithmique

- Comment
   faire apprendre un robot ? (~Maths)

- Comment
   créer une machine intelligente ?

- Comment
   lutter contre les biais algorithmiques ? (~SES)

- Quels
   sont les enjeux de la reconnaissance faciale (notamment éthiques) ?

- Quels
   sont les enjeux de l’intelligence artificielle ?

- “Algorithmes
   : de l’autre côté de la machine”, Aurélie Jean

- Transformation
   d’images : Deep Fakes, une arme de désinformation massive ? / la
   fin de la preuve par l’image ?

- Les
   automates cellulaires (~Maths)

- Du
   neurone au réseau de neurones (~Maths, SVT)

- Les
   systèmes de recommandation (~Maths)

- Enjeux
   autour des langues (traduction, reconnaissance vocale, création)

# Partie 4 : Bases de données

- Données
   personnelles : la vie privée en voie d’extinction ?

- Comment
   optimiser les données ? (~Maths)

# Partie 5 : Architectures matérielles,

systèmes d’exploitation et réseaux

- L’ordinateur
   quantique : nouvelle révolution informatique ?

- La
   course à l’infiniment petit : jusqu’où ? (~PC, Maths)

- Peut-on
   vraiment sécuriser les communications ?

- Cyberguerre
   : la 3ème guerre mondiale ?

- Loi
   de Moore (~PC, Maths)

- L’imagerie
   médicale (~PC, Maths)

- Les
   réseaux, un espace d’innovation exceptionnel (~Maths)

- Bitcoin
   (~Maths, SES)

# Partie 6 : Interfaces Hommes-Machines

(IHM)

- Smart
   cities, smart control ? (~Maths, SES, PC)

- La
   réalité virtuelle : un nouveau monde ?

- Voiture
   autonome, quels enjeux ?

- Robots
   soldats : programmer le permis de tuer ?

- Emploi
   : faut-il craindre le grand remplacement ? (~SES)

- Objets
   connectés (~PC, Maths)

- Systèmes
   embarqués (~PC, Maths)

- L’informatisation
   de la voiture (~Maths, PC)
