# Dictionnaires



## I. Exercices (rappels de 1ère)

### 1. Exercice 1 : cours

Dans un dictionnaire (aussi appelé tableau associatif), chaque élément (appelé valeur) est associé à une clé. Un dictionnaire contient donc des couples **clé : valeur**. Voici les opérations que l'on peut effectuer sur le type abstrait dictionnaire :

- ajout : on associe une nouvelle valeur à une nouvelle clé

- modification : on modifie un couple clé : valeur en remplaçant la valeur courante par une autre valeur (la clé restant identique)

- suppression : on supprime une clé (et donc la valeur qui lui est associée)

- recherche : on recherche une valeur à l'aide de la clé associée à cette valeur

  **1)** Le type dictionnaire est implémenté en Python. Rappelez comment faire pour réaliser chacune des quatre opérations.

### 2. Exercice 2 : exemple

Voici un descriptif des valeurs des lettres au scrabble français :

- 0 point : **Joker** ×2 (appelés en français *jokers* ou *lettres blanches*)

- *1 point* : **E** ×15, **A** ×9, **I** ×8, **N** ×6, **O** ×6, **R** ×6, **S** ×6, **T** ×6, **U** ×6, **L** ×5

- *2 points* : **D** ×3, **M** ×3, **G** ×2

- *3 points* : **B** ×2, **C** ×2, **P** ×2

- *4 points* : **F** ×2, **H** ×2, **V** ×2

- *8 points* : **J** ×1, **Q** ×1

- *10 points* : **K** ×1, **W** ×1, **X** ×1, **Y** ×1, **Z** ×1

​	**1)** Définissez un dictionnaire `score` qui prend les lettres majuscules en clés et leur score en valeur.

​	**2)** Réalisez une fonction `calculer_score ` qui prend une chaîne de caractères `mot` en paramètre et renvoie le score du mot s'il est possible de l'écrire avec les lettres du scrabble et `None` sinon.

​	**3)** Documentez la fonction.

​	**4)** Réalisez deux doctests.

​	**5)** On décide de ne plus jouer avec les jokers.  Donnez l'instruction permettant de les supprimer du dictionnaire `score`.



## II. Tables de hachage

L'implémentation des dictionnaires dans les langages de programmation peut se faire à l'aide des tables de hachage. Les tables de hachage, ainsi que les fonctions de hachage qui sont utilisées pour construire  les tables de hachage, ne sont pas au programme de NSI. Cependant, l'utilisation des fonctions de hachages est omniprésente en informatique, il serait donc bon, pour votre "culture générale  informatique", de connaître le principe des fonctions de hachages. Pour avoir quelques idées sur le principe des tables de hachages, je vous recommande le visionnage de cette vidéo : [wandida : les tables de hachage](https://www.youtube.com/watch?v=CkLctGYWFPA).

Si vous avez visionné la vidéo de wandida, vous avez déjà compris que l'algorithme de recherche dans une table de hachage a une complexité  O(1) : le temps de recherche ne dépend pas du nombre d'éléments présents dans la table de hachage, alors que la complexité de l'algorithme de recherche dans un tableau non trié est O(n). Comme l'implémentation des dictionnaires s'appuie sur les tables de hachage, on peut dire que l'algorithme de recherche d'un élément dans un dictionnaire a une complexité O(1) alors que l'algorithme de recherche d'un élément dans un tableau non trié a une complexité O(n).



## III. Bilan

### 1. Ce qu’il faut savoir

#### Interface des dictionnaires

Dans un dictionnaire, chaque valeur est associée à une clé. Voici les méthodes primitives que l'on peut effectuer sur le type abstrait dictionnaire :

- ajout : on associe une nouvelle valeur à une nouvelle clé
- modification : on modifie un couple clé : valeur en remplaçant la valeur courante par une autre valeur (la clé restant identique)
- suppression : on supprime une clé (et donc la valeur qui lui est associée)
- recherche : on recherche une valeur à l'aide de la clé associée à cette valeur.

#### Implémentation des dictionnaires

Dans beaucoup de langage de programmation les dictionnaires sont implémentés à l’aide de tables de hachage.

#### Algorithme de recherche dans un dictionnaire

L'algorithme de recherche d'un élément dans un dictionnaire a une complexité O(1). La durée de recherche ne dépend pas du nombre d'éléments présents dans le dictionnaire.

### 2. Ce qu’il faut savoir faire

* savoir utiliser les dictionnaires en Python



> **Pour aller plus loin :**
>
> Voici un texte qui vous permettra de comprendre le principe des fonctions de hachages : [c'est quoi le hachage](https://www.culture-informatique.net/cest-quoi-hachage/).