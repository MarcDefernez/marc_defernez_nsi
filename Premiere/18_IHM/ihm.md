<mark>Pour rendre votre travail, utilisez un fichier Jupyter Notebook </mark>

# Interface Homme-Machine

## I. Principe

### 1. Définition

Une **IHM** (Interface Homme - Machine) est un **ensemble de moyens et outils permettant à un humain d'interagir avec une machine**.

Par machine, on entend bien évidemment les **ordinateurs**, mais pas seulement. Les machines peuvent aussi être des **systèmes embarqués** ou des **objets connectés**.

Un ordinateur est une machine utilisant une architecture de Von Neumann. Ainsi, vos tablettes et smartphones sont des ordinateurs !

Un système embarqué est un système qui embarque avec lui de l'informatique et des algorithmes, comme un robot par exemple.

Un objet connecté est un objet qui nécessite une connexion à un réseau et interagit en permanence avec lui, comme un réfrigérateur connecté par exemple.

*Remarque : On abuse souvent de l'appellation objet connecté pour beaucoup d'objets qui n'en sont pas car ils ne sont que rarement branchés sur un réseau et n'interagisse que peu, ou pas du tout, avec lui. Par exemple, la plupart des montres connectées font plutôt parties des systèmes embarqués car elles ne nécessitent pas une connexion permanente au réseau. Seules celles qui doivent être tout le temps connectées au smartphone pour pleinement fonctionner sont réellement des objets connectés.*

> **exercice 1 :**  
> Rappelez ce qu'est l'architecture de Von Neumann.

> **exercice 2 :**  
> Pour chacune des machines suivantes, dites s'il s'agit d'un système embarqué ou d'un objet connecté :
> * thermostat connecté
> * régulateur de vitesse d'une voiture
> * pacemaker
> * assistant vocal (Siri, Alexa, OK Google...)
> * écouteurs Bluetooth
> * photocopieur
> * console de jeux vidéos

### 2. Constituants

Les constituants essentiels d'une IHM sont les **capteurs** et les **actionneurs**.

Un capteur est un composant qui envoient des informations à la machine : les **entrées**. Il capte des données physiques et les convertit en données exploitables par la machine.

Un actionneur est un composant qui modifie le comportement de la machine : les **sorties**. Il convertit les données du programme pour effectuer une action physique.

![capteurs -> machine -> actionneurs](https://framagit.org/JB_info/nsi_1ere/-/raw/main/architectures_materielles/img/capteurs_actionneurs.jpg)

> **exercice 3 :**  
> Prenons par exemple un thermostat connecté.  
> Quels sont ses capteurs ?
> Quels sont ses actionneurs ?

> **exercice 4 :**  
> Mêmes questions pour une console de jeu vidéo (ex : Nintendo Switch).

## II. IHM avec Python

### 1. Interfaces graphiques

En Python, il existe des bibliothèques qui permettent de réaliser des interfaces graphiques. Une **interface graphique** est un outil d'IHM permettant de manipuler des objets à l'écran à l'aide de la souris ou du clavier.

Voici un exemple d'interface graphique pour un jeu de démineur réalisé avec la bibliothèque [tkinter](https://docs.python.org/fr/3/library/tkinter.html) :

![tkinter](https://framagit.org/JB_info/nsi_1ere/-/raw/main/architectures_materielles/img/tkinter.png)

Et un autre exemple d'interface graphique pour un jeu de labyrinthe avec la bibliothèque [pygame](https://www.pygame.org/docs/) :

![pygame](https://framagit.org/JB_info/nsi_1ere/-/raw/main/architectures_materielles/img/pygame.jpg)

*Ces deux exemples font partis de projets que vous réaliserez en Terminale, si vous choisissez de continuer NSI !*

### 2. Interactions simples via la console

Cette année on ne réalisera pas d'interfaces graphiques si on a besoin d'une IHM, car c'est long à programmer.

On réalisera des interactions simples avec l'utilisateur par l'intermédiaire de la console (la fenêtre du bas de Thonny).

Pour cela on aura besoin de 2 fonctions :

1) **print** :
	> **exercice 5 :**  
	> Créez et exécutez un programme (fenêtre du haut) contenant ceci :
	> ```python
	> print("NSI")
	> ```
	> Qu'est-ce qui apparaît dans la console (fenêtre du bas) ?
	
	Le *print* prend en paramètre une valeur (de n'importe quel type) et l'*affiche* dans la console.
	
2) **input** :
	> **exercice 6 :**  
	> Créez et exécutez un programme (fenêtre du haut) contenant ceci :
	> ```python
	> prenom = input("Entrez votre prénom : ")
	> ```
	> Qu'est-ce qui apparaît dans la console (fenêtre du bas) ? Tapez votre prénom dans la console et appuyez sur entrée.
	> Regardez ensuite la valeur de `prenom` (dans la console) :
	> ```python
	> >>> prenom
	> ?
	> ```
	
	Le *input* prend en paramètre un message et récupère la réponse entrée par l'utilisateur.
	

> **exercice 7 :**  
> Parmi le *print* et le *input*, lequel est un capteur ? un actionneur ?

**Il ne faut utiliser print que lorsque la consigne indique explicitement d'*afficher* quelque chose. De même, on utilise input que lorsque la consigne indique explicitement de *demander à l'utilisateur* quelque chose.**

Il ne faut surtout pas confondre print et return. Pareil, on ne doit pas utiliser des input à la place des paramètres de fonction.

<table>
    <tr>        
    	<td align = center colspan = 2>
            <b>Écrivez une fonction qui</b>
        </td>
    </tr>
    <tr>
    	<td align = center style="border-right:solid 5px;">
            <b>prend en paramètre</b> une valeur :
			<pre><code class="language-python">def fonction(valeur):</code></pre>
        </td>
		<td align = center>
            <b>demande à l'utilisateur</b> une valeur :
			<pre><code class="language-python">def fonction():
           valeur = input(...)</code></pre>
        </td>
    </tr>
	    <tr>
    	<td align = center colspan = 2>
            <b>et qui</b>
        </td>
    </tr>
	    <tr>
    	<td align = center style="border-right:solid 5px;">
            <b>renvoie</b> le resultat :
			<pre><code class="language-python">return resultat</code></pre>
        </td>
		<td align = center>
			<b>affiche</b> le résultat :
            <pre><code class="language-python">print(resultat)</code></pre>
        </td>
</table>

> **exercice 8 :**  
> Ecrivez une fonction qui *prend en paramètre* un entier et qui l'*affiche*.

> **exercice 9 :**  
> Ecrivez une fonction qui *demande à l'utilisateur* son prénom, puis son nom, et *renvoie* un tuple contenant les deux.

> **exercice 10 : JEU DU JUSTE PRIX**  
>   
> Dans Jupyter Notebook.  
> 
> * Créez une fonction `nombre_aleatoire` qui prend en paramètre un nombre `maxi` et renvoie un nombre aléatoire compris entre 1 et maxi. *Indice : utilisez la fonction randint de la bibliothèque random. Il faut l'importer comme vous l'avez fait dans le projet.*
> * Créez une fonction `affiche_resultat` qui prend en paramètre deux nombres `reponse` et `tentative` et affiche le message :
> 	* "c'est gagné !" si tentative est égal à réponse
> 	* "c'est plus" si tentative est inférieur à réponse
> 	* "c'est moins" si tentative est supérieur à réponse
> * Créez une fonction `juste prix` qui :
> 	* demande à l'utilisateur un nombre `maxi` qui correspondra à la plus grande valeur possible
> 	* convertit `maxi` en un entier
> 	* tire un nombre aléatoire, appelé `reponse`, entre 1 et `maxi` en utilisant la fonction `nombre_aleatoire`
> 	* initialise à 0 une variable `tentative`
> 	* tant que `tentative` est différent de `réponse`:
> 		* demande à l'utilisateur une valeur `tentative`
> 		* convertit `tentative` en un entier *(fonction int())*
> 		* affiche le résultat en utilisant la fonction `affiche_resultat`
> * Lancez votre fonction `juste_prix` !



## III. IHM avec un système embarqué : la carte micro:bit

### 1. Principe

La carte micro:bit est une carte embarquée qui a été conçue dans un objectif pédagogique.

> **exercice 11 :**  
> En allant visiter [cette page](https://archive.microbit.org/fr/guide/features/), indiquez quels sont les capteurs et actionneurs de la carte micro:bit.

La carte micro:bit se programme en Python.

Emulateur en ligne : [https://create.withcode.uk/](https://create.withcode.uk/).


---

Inspiré du cours de *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [C. Mieszczak](https://framagit.org/tofmzk/informatique_git/)
