REM NSI Scroll Text

SCREEN 0,0,1
WIDTH 40,25
COLOR 0,0,0

PRINT "Principaux thèmes de NSI en première et terminal :"
PRINT "1. Algorithmique et programmation"
PRINT "2. Structure de données"
PRINT "3. Architecture matérielle"
PRINT "4. Langages et programmation web"
PRINT "5. Bases de données"
PRINT "6. Algorithmique avancée"

PRINT "Volume horaire :"
PRINT "Première : 4 heures par semaine"
PRINT "Terminal : 6 heures par semaine"

FOR i = 1 TO 500
  FOR j = 1 TO 500
    NEXT j
  FOR j = 1 TO 500
    NEXT j
  CLS
  SCROLL
NEXT i

END

SUB SCROLL
  ' Défilement du texte
  tmp$ = MID$(screen$(0, 0, 40, 25), 2) + " " + MID$(screen$(0, 0, 40, 25), 1, 1)
  LOCATE 1, 1
  PRINT tmp$;
END SUB
